[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/)
[![Bash](https://img.shields.io/badge/Bash-5.1-be0032)](https://www.gnu.org/software/bash/)


# SWANI

_SWANI_ is a command line program written in [Bash](https://www.gnu.org/software/bash/) to estimate pairwise (dis)similarity measures between (unaligned) genome sequences.

The key aim of _SWANI_ is to provide very accurate pairwise ANI (Average Nucleotide Identity; Konstantinidis & Tiedje 2005, Goris et al. 2007) and _p_-distance (i.e. proportion of nucleotide mismatches) estimates between genomes. More specifically, _SWANI_ implements the OrthoANI approach (Lee et al. 2016) using the Smith-Waterman (SW; 1981) algorithm to obtain accurate orthology assessments and precise similarity measures between the compared nucleotide sequences (see [Methods](#methods)). However, it is worth noting that _SWANI_ requires important running times (e.g. up to five minutes to process a pair of 5 Mbp-long genomes on 48 threads) despite the use of the fast tool [_SWIPE_](https://github.com/torognes/swipe) (Rognes 2011) to run the SW algorithm.

_SWANI_ runs on UNIX, Linux and most OS X operating systems.


## Dependencies

You will need to install the required programs and tools listed in the following tables, or to verify that they are already installed with the required version.


##### Mandatory programs

<div align="center">

| program                                       | package                                                  | version      | sources                                                                                                   |
|:--------------------------------------------- |:--------------------------------------------------------:| ------------:|:--------------------------------------------------------------------------------------------------------- |
| _makeblastdb_                                 | [blast+](https://www.ncbi.nlm.nih.gov/books/NBK279690/)  | &ge; 2.14.0  | [ftp.ncbi.nlm.nih.gov/blast/executables/blast+](https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/)   |
| [_SWIPE_](https://github.com/torognes/swipe)  | -                                                        | &ge; 2.1.1   | [github.com/torognes/swipe](https://github.com/torognes/swipe)                                            |

</div>


##### Standard GNU packages and utilities

<div align="center">

| program                                                                                  | package                                               | version      | sources                                                            |
|:---------------------------------------------------------------------------------------- |:-----------------------------------------------------:| ------------:|:------------------------------------------------------------------ |
| _cut_ <br> _echo_ <br> _fold_ <br> _head_ <br> _join_ <br> _paste_ <br> _sort_ <br> _tr_ | [coreutils](https://www.gnu.org/software/coreutils/)  | > 8.0        | [ftp.gnu.org/gnu/coreutils](https://ftp.gnu.org/gnu/coreutils)     |
| [_gawk_](https://www.gnu.org/software/gawk)                                              | -                                                     | > 4.0.0      | [ftp.gnu.org/gnu/gawk](http://ftp.gnu.org/gnu/gawk/)               |
| [_grep_](https://www.gnu.org/software/grep)                                              | -                                                     | > 2.0        | [ftp.gnu.org/gnu/grep](https://ftp.gnu.org/gnu/grep)               |
| [_sed_](https://www.gnu.org/software/sed)                                                | -                                                     | > 4.2        | [ftp.gnu.org/gnu/sed](https://ftp.gnu.org/gnu/sed)                 |

</div>



## Installation and execution

**A.** Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/SWANI.git
```

**B.** Go to the created directory to give the execute permission to the file: 

```bash
cd SWANI/
chmod +x SWANI.sh
```
**C.** Check the dependencies (and their version) using the following command line:

```bash
./SWANI.sh  -c
```

**D.** If at least one of the required program (see [Dependencies](#dependencies)) is not available on your `$PATH` variable (or if one compiled binary has a different default name), it should be manually specified.
To specify the location of a specific binary, edit the file `SWANI.sh` and indicate the local path to the corresponding binary(ies) within the code block `REQUIREMENTS` (approximately lines 50-80).

**E.** Execute _SWANI_ with the following command line model:

```bash
./SWANI.sh [options]
```




## Usage

Run _SWANI_ without option to read the following documentation:

```
 USAGE:  SWANI  [options]  <infile> <infile> [<infile> ...]

 OPTIONS:
  -l <int>   segment length (default: 1020 bps)
  -o <file>  outfile name (default: none)
  -w <dir>   path to the tmp directory (default: $TMPDIR, otherwise /tmp)
  -t <int>   thread numbers (default: 2)
  -v         verbose mode (default: not set)
  -c         checks dependencies and exit
  -h         prints this help and exit
```


## Notes

* Each input file should be in FASTA format and may contain nucleotide sequences. At least two input files should be specified. If more than two files are specified, the pairwise similarities are computed between the genome in the first file and the genome in each other files.

* Faster running times can be observed when using a large number of threads (option `-t`). In general, it is recommended to use at least 12 threads.

* By default, a progress bar and the tab-delimited results are outputted in _stderr_ and _stdout_, respectively. The progress bar can be suppressed by ending the command line with `2>/dev/null`. When using option `-v`, the progess bar is replaced with detailed information.



## Methods

Each input genome nucleotide sequence is decomposed into a set of consecutive segments, each of length (at most) _L_&nbsp;=&nbsp;1020 bps (Goris et al. 2007, Lee et al. 2016).
Of note, the length _L_ can be modified using option `-l`.
_SWANI_ extracts segments containing only the character states A, C, G and T (case insensitive), and discards all segments of length smaller than 0.1&thinsp;_L_&nbsp; bps.

Given two sets of nucleotide segments (one per genome), reciprocal similarity searches are performed, and orthologous segment pairs are assessed by Reciprocal Best Hit (RBH; Lee et al. 2016).
It is worth noting that every RBH should include at least 0.1&thinsp;_L_&nbsp; bps of each of the two involved segments.
In order to deal with a wide range of pairwise fragment distances, this reciprocal similarity search step is performed using [_SWIPE_](https://github.com/torognes/swipe) (Rognes 2011) with very relaxed scoring parameters:


<div align="center">
<sub>

| reward |  penalty  | gap open | gap extend |
|:------:|:---------:|:--------:|:----------:|
|   2    | &minus;1  |    3     |      1     |

</sub>
</div>

In order to obtain accurate nucleotide (dis)similarity values, each pair of orthologous segments is next re-aligned using [_SWIPE_](https://github.com/torognes/swipe) with scoring parameters selected from the initial nucleotide similarity estimate (derived from the RBH results):

<div align="center">
<sub>

| expected <br> nucleotide similarity | reward |  penalty  | gap <br> open | gap <br> extend |
|:-----------------------------------:|:------:|:---------:|:-------------:|:---------------:|
| [ 98% - 100% ]                      |   1    | &minus;3  |       12      |        8        |
| [ 96% - 98% [                       |   2    | &minus;5  |       21      |       14        |
| [ 93% - 96% [                       |   1    | &minus;2  |        9      |        6        |
| [ 90% - 93% [                       |   2    | &minus;3  |       15      |       10        |
| [ 85% - 90% [                       |   3    | &minus;4  |       21      |       14        |
| [ 80% - 85% [                       |   4    | &minus;5  |       27      |       18        |
| [ 75% - 80% [                       |   1    | &minus;1  |        6      |        4        |
| [ 70% - 75% [                       |   9    | &minus;8  |       51      |       34        |
| [ 65% - 70% [                       |   5    | &minus;4  |       27      |       18        |
| [ 60% - 65% [                       |   8    | &minus;6  |       42      |       28        |
| [ 55% - 60% [                       |   3    | &minus;2  |       15      |       10        |
| [ 0% - 55% [                        |   7    | &minus;4  |       33      |       22        |

</sub>
</div>

The relationship between expected nucleotide similarity ranges and recommended reward/penalty score ratios are derived from States et al. (1991).
Large gap open/extend costs are used to obtain local alignments with maximized ungapped regions, as gapped regions are useless when estimating pairwise nucleotide (dis)similarity.

Each pairwise local alignment _i_ of orthologous nucleotide sequences includes <i>m<sub>i</sub></i> aligned positions with identical character states and <i>x<sub>i</sub></i> aligned positions with different (non-gap) character states.
Denoting <i>l<sub>i</sub></i>&nbsp;:=&nbsp;<i>m<sub>i</sub></i>&nbsp;+&nbsp;<i>x<sub>i</sub></i>, the ANI and _p_-distance values are obtained using the following formulae:

<div align="center">

%ANI&ensp;:=&ensp;100&nbsp;&times;&nbsp;(&thinsp;&Sum;<sub><sub><i>i</i>&thinsp;=&thinsp;1...<i>n</i></sub></sub>&ensp;<i>m<sub>i</sub></i>&nbsp;/&nbsp;<i>l<sub>i</sub></i>&thinsp;)&nbsp;/&nbsp;<i>n</i>

_p_-distance&ensp;:=&ensp;1&nbsp;&minus;&nbsp;(&thinsp;&Sum;<sub><sub><i>i</i>&thinsp;=&thinsp;1...<i>n</i></sub></sub>&ensp;<i>m<sub>i</sub></i>&thinsp;)&nbsp;/&nbsp;(&thinsp;&Sum;<sub><sub><i>i</i>&thinsp;=&thinsp;1...<i>n</i></sub></sub>&ensp;<i>l<sub>i</sub></i>&thinsp;)

</div>

As <i>s<sub>i</sub></i>&nbsp;:=&nbsp;<i>m<sub>i</sub></i>&nbsp;/&nbsp;<i>l<sub>i</sub></i> is the nucleotide similarity induced by the local alignment _i_, the ANI value is therefore the average of the _n_ similarity measures <i>s<sub>i</sub></i> (Lee et al. 2016).
In order to give more weight to long local alignments (i.e. large <i>l<sub>i</sub></i> values), the weighted average of the similarity measures <i>s<sub>i</sub></i> is given by (&thinsp;&Sum;<sub><sub><i>i</i>&thinsp;=&thinsp;1...<i>n</i></sub></sub>&ensp;<i>l<sub>i</sub>&nbsp;s<sub>i</sub></i>&thinsp;)&nbsp;/&nbsp;(&thinsp;&Sum;<sub><sub><i>i</i>&thinsp;=&thinsp;1...<i>n</i></sub></sub>&ensp;<i>l<sub>i</sub></i>&thinsp;).
The _p_-distance is therefore the reciprocal of the weighted average of the pairwise similarity measures <i>s<sub>i</sub></i>.
In consequence, one generally observes 1&nbsp;&minus;&nbsp;_p_-distance&nbsp;&gt;&nbsp;%ANI&nbsp;/&nbsp;100.



## Simulation results

In order to illustrate the accuracy of _SWANI_, it was run on 12,000 pairs of nucleotide sequences as simulated by Criscuolo (2020) using the nucleotide substitution model GTR+&Gamma; (available at [zenodo.org/records/4034462](https://zenodo.org/records/4034462)).

In brief, varying _d_ from 0.05 to 1.00 (step = 0.05), a total of 200 nucleotide sequence pairs with _d_ substitution events per character were simulated under the model GTR+Γ (with relative rates of nucleotide substitution and &Gamma; shape parameters drawn from real-case phylogenetic inferences).
Each model was adjusted with indel events (leading to sequence lengths varying from 2.04 to 5.69&nbsp;Mbps) and three different equilibrium frequencies: equal frequencies (<i>f</i><sub>1</sub>; &pi;<sub>A</sub> = &pi;<sub>C</sub> = &pi;<sub>G</sub> = &pi;<sub>T</sub> = 25%), GC-rich (<i>f</i><sub>2</sub>; &pi;<sub>A</sub>&nbsp;=&nbsp;10%, &pi;<sub>C</sub>&nbsp;=&nbsp;30%, &pi;<sub>G</sub>&nbsp;=&nbsp;40%, &pi;<sub>T</sub>&nbsp;=&nbsp;20%), and AT-rich (<i>f</i><sub>3</sub>; &pi;<sub>A</sub>&nbsp;=&nbsp;&pi;<sub>T</sub>&nbsp;=&nbsp;40%, &pi;<sub>C</sub>&nbsp;=&nbsp;&pi;<sub>G</sub>&nbsp;=&nbsp;10%), leading to 3 (<i>f</i><sub>1</sub>, <i>f</i><sub>2</sub>, <i>f</i><sub>3</sub>) &times; 20 (_d_&nbsp;=&nbsp;0.05, 0.10, ..., 1.00) &times; 200 = 12,000 simulated sequence pairs with known dissimilarity (i.e. true _p_-distances value).

The figure below represents the ANI values returned by _SWANI_ (default options) against the known pairwise similarities (i.e. 1 minus the true _p_-distance value), using blue, orange and red dots for equilibrium frequencies <i>f</i><sub>1</sub>, <i>f</i><sub>2</sub> and <i>f</i><sub>3</sub>, respectively.
It shows that the ANI values returned by _SWANI_ are quite correlated with the true similarity values (especially when ANI > 90%, as expected by definition).

<div align="center"><img src="figures/SWANI.ANI.svg" alt="SWANI ANI" width="50%"/></div>

For comparison, the next figure represents the ANI values returned by [_fastANI_](https://github.com/ParBLiSS/FastANI) v1.33 (Jain et al. 2018) on the same dataset (default options), showing an important downward bias in most cases.

<div align="center"><img src="figures/fastANI.svg" alt="fastANI" width="50%"/></div>

The third figure represents the _p_-distance values returned by _SWANI_ (default options) against the known pairwise dissimilarities.
It clearly shows that _SWANI_ is able to accurately estimate the true _p_-distance (up to 0.35) in many conditions.

<div align="center"><img src="figures/SWANI.p-distance.svg" alt="SWANI p-distance" width="50%"/></div>

For comparison, the last figure represents the _p_-distance values returned by [_Mash_](https://github.com/marbl/Mash) v2.3 (Ondov et al. 2016).
Of note, [_Mash_](https://github.com/marbl/Mash) was run with _k_&nbsp;=&nbsp;25 and a large sketch of size 3,000,000, and every distance value _d_ returned by [_Mash_](https://github.com/marbl/Mash) was transformed into an estimated _p_-distance _p_ using the formula <i>p</i>&nbsp;=&nbsp;1&nbsp;&minus;&nbsp;<i>e<sup>&minus;d</sup></i> (see e.g. Criscuolo 2020).
This figure shows that the _p_-distance approximations that can be obtained using [_Mash_](https://github.com/marbl/Mash) are often suffering from an upper bias, generally caused by the indel events.

<div align="center"><img src="figures/mash.svg" alt="mash" width="50%"/></div>




## Real case results

The following examples compare the estimates of _SWANI_ with those ouputted by several other tools from different pairs of bacteria genomes.

##### Downloading genome sequences

Download the 12 pairs of genome sequence files using the following [Bash](https://www.gnu.org/software/bash/) command lines:

```bash
URL="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA";

wget -q -O - $URL/000/237/545/GCA_000237545.2_ASM23754v2/GCA_000237545.2_ASM23754v2_genomic.fna.gz | gunzip -c > Vibrio.cholerae.2010EL-2010H.fasta ;
wget -q -O - $URL/000/765/415/GCA_000765415.1_ASM76541v1/GCA_000765415.1_ASM76541v1_genomic.fna.gz | gunzip -c > Vibrio.cholerae.2012EL-2176.fasta ;

wget -q -O - $URL/000/006/945/GCA_000006945.1_ASM694v1/GCA_000006945.1_ASM694v1_genomic.fna.gz     | gunzip -c > Salmonella.enterica.LT2.fasta ;
wget -q -O - $URL/000/603/785/GCA_000603785.1_ASM60378v1/GCA_000603785.1_ASM60378v1_genomic.fna.gz | gunzip -c > Salmonella.enterica.CVM24391.fasta ;

wget -q -O - $URL/000/581/175/GCA_000581175.1_ASM58117v1/GCA_000581175.1_ASM58117v1_genomic.fna.gz | gunzip -c > Acinetobacter.baumannii.1465485.fasta ;
wget -q -O - $URL/000/162/295/GCA_000162295.1_ASM16229v1/GCA_000162295.1_ASM16229v1_genomic.fna.gz | gunzip -c > Acinetobacter.baumannii.ATCC19606.fasta ;

wget -q -O - $URL/000/239/035/GCA_000239035.2_ASM23903v2/GCA_000239035.2_ASM23903v2_genomic.fna.gz | gunzip -c > Mycobacteroides.abscessus.BD.fasta ;
wget -q -O - $URL/000/260/575/GCA_000260575.1_ASM26057v1/GCA_000260575.1_ASM26057v1_genomic.fna.gz | gunzip -c > Mycobacteroides.abscessus.M154.fasta ;

wget -q -O - $URL/000/387/405/GCA_000387405.2_bt_cbt_v2.0/GCA_000387405.2_bt_cbt_v2.0_genomic.fna.gz   | gunzip -c > Bacillus.thuringiensis.T01-328.fasta ;
wget -q -O - $URL/000/600/315/GCA_000600315.1_Newbler2.5.3/GCA_000600315.1_Newbler2.5.3_genomic.fna.gz | gunzip -c > Bacillus.thuringiensis.NBIN-866.fasta ;

wget -q -O - $URL/000/294/495/GCA_000294495.1_ASM29449v1/GCA_000294495.1_ASM29449v1_genomic.fna.gz | gunzip -c > Streptococcus.suis.S735.fasta ;
wget -q -O - $URL/000/441/215/GCA_000441215.1_version1/GCA_000441215.1_version1_genomic.fna.gz     | gunzip -c > Streptococcus.suis.YS35.fasta ;

wget -q -O - $URL/000/007/565/GCA_000007565.1_ASM756v1/GCA_000007565.1_ASM756v1_genomic.fna.gz     | gunzip -c > Pseudomona.putida.KT2440.fasta ;
wget -q -O - $URL/000/412/675/GCA_000412675.1_ASM41267v1/GCA_000412675.1_ASM41267v1_genomic.fna.gz | gunzip -c > Pseudomona.putida.NBRC14164.fasta ;

wget -q -O - $URL/000/330/825/GCA_000330825.1_SASA1.0/GCA_000330825.1_SASA1.0_genomic.fna.gz             | gunzip -c > Staphylococcus.aureus.DSM20231.fasta ;
wget -q -O - $URL/000/752/015/GCA_000752015.1_Sa_H115100079/GCA_000752015.1_Sa_H115100079_genomic.fna.gz | gunzip -c > Staphylococcus.argenteus.H115100079.fasta ;

wget -q -O - $URL/000/195/835/GCA_000195835.1_ASM19583v1/GCA_000195835.1_ASM19583v1_genomic.fna.gz | gunzip -c > Mycobacterium.tuberculosis.AF2122_97.fasta ;
wget -q -O - $URL/000/240/485/GCA_000240485.2_ASM24048v2/GCA_000240485.2_ASM24048v2_genomic.fna.gz | gunzip -c > Mycobacterium.avium.DT78.fasta ;

wget -q -O - $URL/000/400/385/GCA_000400385.1_ASM40038v1/GCA_000400385.1_ASM40038v1_genomic.fna.gz | gunzip -c > Vibrio.owensii.CAIM1854.fasta ;
wget -q -O - $URL/000/788/345/GCA_000788345.1_ASM78834v1/GCA_000788345.1_ASM78834v1_genomic.fna.gz | gunzip -c > Vibrio.vulnificus.ATL71503.fasta ;

wget -q -O - $URL/000/147/295/GCA_000147295.1_ASM14729v1/GCA_000147295.1_ASM14729v1_genomic.fna.gz                         | gunzip -c > Enterococcus.faecalis.TX4244.fasta ;
wget -q -O - $URL/000/407/405/GCA_000407405.1_Ente_flav_ATCC49996_V2/GCA_000407405.1_Ente_flav_ATCC49996_V2_genomic.fna.gz | gunzip -c > Enterococcus.casseliflavus.ATCC49996.fasta ;

wget -q -O - $URL/000/167/335/GCA_000167335.1_ASM16733v1/GCA_000167335.1_ASM16733v1_genomic.fna.gz | gunzip -c > Bacillus.anthracis.Australia94.fasta ;
wget -q -O - $URL/000/420/205/GCA_000420205.1_ASM42020v1/GCA_000420205.1_ASM42020v1_genomic.fna.gz | gunzip -c > Heyndrickxia.coagulans.ATCC7050.fasta ;

```

##### Running _SWANI_

For the second pair of _Salmonella enterica_ genomes, running _SWANI_ using 48 threads, e.g.

```bash
SWANI.sh -t 48 Salmonella.enterica.LT2.fasta Salmonella.enterica.CVM24391.fasta
```

leads to the following output, i.e. first and second files, no. fragments from the first and second genomes, no. orthologous fragment pairs, _p_-distance and %ANI:

```
Salmonella.enterica.LT2.fasta  Salmonella.enterica.CVM24391.fasta  4855  4809  4486  0.009927  98.8189
```

The table below shows the ANI values estimated between different genome pairs, as published by Lee et al. (2016) for _OrthoANI_.
The last columns shows the ANI and _p_-distance measures obtained using _SWANI_ (default options), [_fastANI_](https://github.com/ParBLiSS/FastANI) v1.33 (default options) and [_Mash_](https://github.com/marbl/Mash) v2.3 (_k_&nbsp;=21 and sketch size&nbsp;=&nbsp;3,000,000).

<div align="center">
<sub>

| genome 1                                                      | genome 2                                                      | _OrthoANI_ <br> %ANI | _SWANI_ <br> %ANI | _fastANI_ <br> %ANI | _SWANI_ <br> _p_-distance | _Mash_ <br> _p_-distance |
|:------------------------------------------------------------- |:------------------------------------------------------------- |:--------------------:|:-----------------:|:-------------------:|:-------------------------:|:------------------------:|
| _Vibrio cholerae_ 2010EL-2010H         <br> (GCA_000237545.2) | _Vibrio cholerae_ 2012EL-2176          <br> (GCA_000765415.1) |  99.9995             |  99.9924          |  99.9939            |  0.000097                 |  0.001059                |
| _Salmonella enterica_ LT2              <br> (GCA_000006945.1) | _Salmonella enterica_ CVM24391         <br> (GCA_000603785.1) |  99.0005             |  98.8189          |  98.9790            |  0.009927                 |  0.009911                |
| _Acinetobacter baumannii_ 1465485      <br> (GCA_000581175.1) | _Acinetobacter baumannii_ ATCC19606    <br> (GCA_000162295.1) |  98.0006             |  97.7229          |  97.8369            |  0.020525                 |  0.021028                |
| _Mycobacteroides abscessus_ BD         <br> (GCA_000239035.2) | _Mycobacteroides abscessus_ M154       <br> (GCA_000260575.1) |  97.0065             |  97.0008          |  96.8003            |  0.029356                 |  0.029716                |
| _Bacillus thuringiensis_ T01-328       <br> (GCA_000387405.2) | _Bacillus thuringiensis_ NBIN-866      <br> (GCA_000600315.1) |  96.0199             |  95.8910          |  95.8419            |  0.037287                 |  0.038036                |
| _Streptococcus suis_ S735              <br> (GCA_000294495.1) | _Streptococcus suis_ YS35              <br> (GCA_000441215.1) |  95.0231             |  94.7476          |  95.1343            |  0.048395                 |  0.043161                |
| _Pseudomona putida_ KT2440             <br> (GCA_000007565.1) | _Pseudomona putida_ NBRC14164          <br> (GCA_000412675.1) |  90.0915             |  90.0227          |  90.2671            |  0.097766                 |  0.083816                |
| _Staphylococcus aureus_ DSM20231       <br> (GCA_000330825.1) | _Staphylococcus argenteus_ H115100079  <br> (GCA_000752015.1) |  87.7674             |  88.1305          |  88.5696            |  0.119349                 |  0.085446                |
| _Mycobacterium tuberculosis_ AF2122/97 <br> (GCA_000195835.1) | _Mycobacterium avium_ DT78             <br> (GCA_000240485.2) |  80.1248             |  80.5896          |  81.0143            |  0.192916                 |  0.172071                |
| _Vibrio owensii_ CAIM1854              <br> (GCA_000400385.1) | _Vibrio vulnificus_ ATL71503           <br> (GCA_000788345.1) |  75.0192             |  74.3493          |  80.2788            |  0.253023                 |  0.180977                |
| _Enterococcus faecalis_ TX4244         <br> (GCA_000147295.1) | _Enterococcus.casseliflavus_ ATCC49996 <br> (GCA_000407405.1) |  70.4920             |  69.5141          |  -                  |  0.301522                 |  0.232544                |
| _Bacillus anthracis_ Australia94       <br> (GCA_000167335.1) | _Heyndrickxia coagulans_ ATCC7050      <br> (GCA_000420205.1) |  65.5017             |  64.2155          |  -                  |  0.361460                 |  0.311488                |

</sub>
</div>



## References

Criscuolo (2020) _On the transformation of MinHash-based uncorrected distances into proper evolutionary distances for phylogenetic inference_. **F1000Research**, 9:1300. [doi:10.12688/f1000research.26930.1](https://doi.org/10.12688/f1000research.26930.1)

Goris J, Konstantinidis KT, Klappenbach JA, Coenye T, Vandamme P, Tiedje JM (2007) _DNA-DNA hybridization values and their relationship to whole-genome sequence similarities_. **International Journal of Systematic and Evolutionary Biology**, 57(1):81-91. [doi:10.1099/ijs.0.64483-0](https://doi.org/10.1099/ijs.0.64483-0)

Jain C, Rodriguez-R LM, Phillippy AM, Konstantinidis KT, Aluru S (2018) _High throughput ANI analysis of 90K prokaryotic genomes reveals clear species boundaries_. **Nature Communications**, 9:5114. [doi:10.1038/s41467-018-07641-9](https://doi.org/10.1038/s41467-018-07641-9)

Konstantinidis KT, Tiedje JM (2005) _Genomic insights that advance the species definition for prokaryotes_. **Proceedings of the National Academy of Sciences of the United States of America**, 102(7):2567-2572. [doi:/10.1073/pnas.0409727102 ](https://doi.org/10.1073/pnas.0409727102)

Lee I, Kim YO, Park S-C, Chun J (2016) _OrthoANI: An improved algorithm and software for calculating average nucleotide identity_. **International Journal of Systematic and Evolutionary Biology**, 66(2):1100-1103. [doi:10.1099/ijsem.0.000760](https://doi.org/10.1099/ijsem.0.000760)

Ondov BD, Treangen TJ, Melsted P, Mallonee AB, Bergman NH, Koren S, Phillippy AM (2016) _Mash: fast genome and metagenome distance estimation using MinHash_. **Genome Biology**, 17(1):132. [doi:10.1186/s13059-016-0997-x](https://doi.org/10.1186/s13059-016-0997-x).

Rognes T (2011) _Faster Smith-Waterman database searches with inter-sequence SIMD parallelisation_. **BMC Bioinformatics**, 12:221. [doi:10.1186/1471-2105-12-221](https://doi.org/10.1186/1471-2105-12-221)

Smith TF, Waterman MS (1981) _Identification of Common Molecular Subsequences_. **Journal of Molecular Biology**, 147(1):195-197. [doi:10.1016/0022-2836(81)90087-5](https://doi.org/10.1016%2F0022-2836%2881%2990087-5)

States DJ, Gish W, Altschul SF (1991) _Improved sensitivity of nucleic acid database searches using application-specific scoring matrices_. **Methods**, 3:66-70. [doi:10.1016/S1046-2023(05)80165-3](https://doi.org/10.1016/S1046-2023(05)80165-3)
