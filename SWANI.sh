#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  SWANI: Smith-Waterman-based Average Nucleotide Identity                                                   #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2024 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.0;                                                                                               #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# - SUMMARY -                                                                                                #
# gawk/5.0.1 blast+/2.15.0 swipe/2.1.1
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
#                                                                                                            #
# -- makeblastdb ------------------------------------------------------------------------------------------  #
#                                                                                                            #
  MAKEBLASTDB_BIN=makeblastdb;
#                                                                                                            #
# -- SWIPE ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  SWIPE_BIN=swipe;
#                                                                                                            #
# -- GNU tools --------------------------------------------------------------------------------------------  #
#                                                                                                            #
#  core-utils: cut  echo  fold  head  join  paste  sort  tr
#  others:     grep  sed 
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- n/a --------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
# -- scores -----------------------------------------------------------------------------------------------  #
#                                                                                                            #
# Every SWIPE parameter is summarized below, with each pair of reward,penalty scores associated to a range   #
# of expected pairwise nucleotide similarities.                                                              #
#                                                                                                            #
# Derived from: Table D1 at www.ncbi.nlm.nih.gov/books/NBK279684/                                            #
#               Tables 1 & 2 in States et al. (1991) doi:10.1016/S1046-2023(05)80165-3                       #
#                                                                                                            #
#  %similarity   |reward/penalty|   open/extend gap costs  ||  open   := (reward-penalty) * 3                #
#  -----------   ----------------   ---------------------  ||  extend := (reward-penalty) * 2                #
#  [0  - 55 [    |7/-4| = 1.750     33/22                                                                    #
#  [55 - 60 [    |3/-2| = 1.500     15/10                                                                    #
#  [60 - 65 [    |8/-6| = 1.333     42/28                                                                    #
#  [65 - 70 [    |5/-4| = 1.250     27/18                                                                    #
#  [70 - 75 [    |9/-8| = 1.125     51/34                                                                    #
#  [75 - 80 [    |1/-1| = 1.000     6/4                                                                      #
#  [80 - 85 [    |4/-5| = 0.800     27/18                                                                    #
#  [85 - 90 [    |3/-4| = 0.750     21/14                                                                    #
#  [90 - 93 [    |2/-3| = 0.666     15/10                                                                    #
#  [93 - 96 [    |1/-2| = 0.500     9/6                                                                      #
#  [96 - 98 [    |2/-5| = 0.400     21/14                                                                    #
#  [98 - 100]    |1/-3| = 0.333     12/8                                                                     #
#                                                                                                            #
  SCORES_INIT="  --reward=2  --penalty=-1  --gapopen=3  --gapextend=1 ";
  SCORES_0_55="  --reward=7  --penalty=-4  --gapopen=33 --gapextend=22";
  SCORES_55_60=" --reward=3  --penalty=-2  --gapopen=15 --gapextend=10";
  SCORES_60_65=" --reward=8  --penalty=-6  --gapopen=42 --gapextend=28";
  SCORES_65_70=" --reward=5  --penalty=-4  --gapopen=27 --gapextend=18";
  SCORES_70_75=" --reward=9  --penalty=-8  --gapopen=51 --gapextend=34";
  SCORES_75_80=" --reward=1  --penalty=-1  --gapopen=6  --gapextend=4 ";
  SCORES_80_85=" --reward=4  --penalty=-5  --gapopen=27 --gapextend=18";
  SCORES_85_90=" --reward=3  --penalty=-4  --gapopen=21 --gapextend=14";
  SCORES_90_93=" --reward=2  --penalty=-3  --gapopen=15 --gapextend=10";
  SCORES_93_96=" --reward=1  --penalty=-2  --gapopen=9  --gapextend=6 ";
  SCORES_96_98=" --reward=2  --penalty=-5  --gapopen=21 --gapextend=14";
  SCORES_98_100="--reward=1  --penalty=-3  --gapopen=12 --gapextend=8 ";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = FITTINGS     =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BAWK="$GAWK_BIN";
  CAWK="$GAWK_BIN -F,";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- makeblastdb ------------------------------------------------------------------------------------------  #
#                                                                                                            #
  MAKEBDB="$MAKEBLASTDB_BIN -input_type fasta -dbtype nucl -blastdb_version 4 -parse_seqids";
#                                                                                                            #
# -- SWIPE ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  SWIPE_STATIC_OPTIONS="--symtype=blastn --num_descriptions=2 --num_alignments=2 --evalue=100.0 --outfmt=8";
  SWIPE="$SWIPE_BIN $SWIPE_STATIC_OPTIONS";
#                                                                                                            #
##############################################################################################################

  
##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m SWANI v$VERSION                           $COPYRIGHT\033[0m";
  cat <<EOF

 Smith-Waterman-based Average Nucleotide Identity
 https://gitlab.pasteur.fr/GIPhy/SWANI

 USAGE:  SWANI  [options]  <infile> <infile> [<infile> ...]

 OPTIONS:
  -l <int>   segment length (default: 1020 bps)
  -o <file>  outfile name (default: none)
  -w <dir>   path to the tmp directory (default: \$TMPDIR, otherwise /tmp)
  -t <int>   thread numbers (default: 2)
  -v         verbose mode (default: not set)
  -c         checks dependencies and exit
  -h         prints this help and exit

EOF
} 
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- echoxit() --------------------------------------------------------------------------------------------- #
# >> prints in stderr the specified error message $1 and next exit 1                                         #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ;
  exit 1 ;
}    
#                                                                                                            #
# -- dcontrol ---------------------------------------------------------------------------------------------  #
# >> controls required dependancies                                                                          #
#                                                                                                            #
dcontrol() {
  local fail=false;
  for binexe in  $GAWK_BIN  $MAKEBLASTDB_BIN  $SWIPE_BIN
  do
    if [ ! $(command -v $binexe) ]
    then
      echo "[ERROR] $binexe not found" >&2 ;
      fail=true;
    fi
  done
  if $fail ; then exit 1 ; fi
}
#                                                                                                            #
# -- dcheck -----------------------------------------------------------------------------------------------  #
# >> checks (non-coreutils) dependancies and exit                                                            #
#                                                                                                            #
dcheck() {
  echo "Checking required dependencies ..." ;
  ## grep ############################
  echo -e -n "> \e[1mgrep\e[0m        >1.0       mandatory\t" ;       binexe=grep;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   grep -V | head -1 ;
  fi
  ## sed #############################
  echo -e -n "> \e[1msed\e[0m         >1.0       mandatory\t" ;       binexe=sed;
  echo -e -n "$binexe     \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   sed --version | head -1 ;
  fi
  ## gawk >=4.0.0 ####################
  echo -e -n "> \e[1mgawk\e[0m        >4.0.0     mandatory\t" ;       binexe=$GAWK_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $GAWK_BIN -V | head -1 ;
  fi
  ## makeblastdb >=2.14.0 ############
  echo -e -n "> \e[1mmakeblastdb\e[0m >=2.14.0   mandatory\t" ;       binexe=$MAKEBLASTDB_BIN;
  echo -e -n "$binexe\t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $MAKEBLASTDB_BIN -version | head -1 ;
  fi
  ## SWIPE >=2.1.1 ###################
  echo -e -n "> \e[1mSWIPE\e[0m       >=2.1.1    mandatory\t" ;       binexe=$SWIPE_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $SWIPE_BIN -h | head -1 ;
  fi
  echo "[exit]" ;
  exit ;
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# INITIALIZING PARAMETERS AND READING OPTIONS                                                                #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

export BLAST_USAGE_REPORT=false;
export LC_ALL=C;

WLGT=1020;                 # window length  -l
OUTFILE="$NA";             # outfile name   -o
TMP_DIR=${TMPDIR:-/tmp};   # tmp directory  -w
NTHREADS=2;                # no. threads    -t
VERBOSE=false;             # verbose mode   -v        
DEBUG=false;               # debug mode     -X

while getopts l:o:w:t:cvhX option
do
  case $option in
  l)  WLGT=$OPTARG       ;;
  o)  OUTFILE="$OPTARG"  ;;
  w)  TMP_DIR="$OPTARG"  ;;
  t)  NTHREADS=$OPTARG   ;;
  v)  VERBOSE=true       ;;
  c)  dcheck             ;;
  X)  DEBUG=true         ;;
  h)  mandoc ;  exit 0   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done

$DEBUG && VERBOSE=true;

if $VERBOSE
then
  echo "# SWANI v$VERSION" ;
  echo "# $COPYRIGHT" ;
  echo "+ https://gitlab.pasteur.fr/GIPhy/SWANI" ;
  echo "> Syst: $MACHTYPE" ;
  echo "> Bash: $BASH_VERSION" ;
fi

dcontrol;

[[ $WLGT =~ ^[0-9]+$ ]]             || echoxit "[ERROR] incorrect value (option -l): $WLGT" ; 
[[ $NTHREADS =~ ^[0-9]+$ ]]         || echoxit "[ERROR] incorrect value (option -t): $NTHREADS" ; 
 [ $NTHREADS -lt 1 ] && NTHREADS=1;

MINLGT=$(( $WLGT / 10 ));
 


##############################################################################################################
#                                                                                                            #
# CHECKING INFILES                                                                                           #
#                                                                                                            #
##############################################################################################################

shift "$(( $OPTIND - 1 ))"
NFILE=$#;      # no. input file(s)
FLIST="$@";    # specified input file(s)

ouch=false;
[ $NFILE -eq 0 ] && echoxit "[ERROR] no input file" ;
for f in $FLIST
do
  if [ ! -e $f ]; then echo "[ERROR] file not found: $f"     >&2 ; ouch=true; continue; fi
  if [   -d $f ]; then echo "[ERROR] not a file: $f"         >&2 ; ouch=true; continue; fi
  if [ ! -s $f ]; then echo "[ERROR] empty file: $f"         >&2 ; ouch=true; continue; fi
  if [ ! -r $f ]; then echo "[ERROR] no read permission: $f" >&2 ; ouch=true; continue; fi
done
$ouch && exit 1 ;
[ $NFILE -eq 1 ] && echoxit "[ERROR] only one input file" ;

FASTA1=$1;     # first input file

shift 1 ;

NFILE=$#;      # no. remaining input file(s)
FLIST="$@";    # remaining input file(s)


##############################################################################################################
#                                                                                                            #
# CREATING TMP DIRECTORY AND DEFINING TRAP AND TMP FILES                                                     #
#                                                                                                            #
##############################################################################################################

[ "${TMP_DIR:0:1}" != "/" ] && TMP_DIR="$(pwd)/$TMP_DIR";
if [ ! -e $TMP_DIR ]; then echo ; echo "[ERROR] tmp directory does not exist (option -w): $TMP_DIR" >&2 ; exit 1 ; fi
if [ ! -d $TMP_DIR ]; then echo ; echo "[ERROR] not a directory (option -w): $TMP_DIR"              >&2 ; exit 1 ; fi
if [ ! -w $TMP_DIR ]; then echo ; echo "[ERROR] no write permission (option -w): $TMP_DIR"          >&2 ; exit 1 ; fi

TMP_DIR=$(mktemp -d -p $TMP_DIR swani.XXXXXXXXXX);
finalize() { rm -r $TMP_DIR ; }
trap 'finalize ; exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

SEQ1=$TMP_DIR/seq1.fasta;
SEQ2=$TMP_DIR/seq2.fasta;
SFRAG1=$TMP_DIR/sfrag1.fasta;
SFRAG2=$TMP_DIR/sfrag2.fasta;
SEQI=$TMP_DIR/seqi.fasta;
SEQJ=$TMP_DIR/seqj.fasta;
SFRAGI=$TMP_DIR/sfragi.fasta;
SFRAGJ=$TMP_DIR/sfragj.fasta;
FRAGI=$TMP_DIR/fragi.fasta;
FRAGJ=$TMP_DIR/fragj.fasta;
TABIJ=$TMP_DIR/tabij.txt;
TABJI=$TMP_DIR/tabji.txt;
TABRBH=$TMP_DIR/tabrbh.txt;
TMP=$TMP_DIR/tmp.txt;
TAX=$TMP_DIR/tax.txt;


##############################################################################################################
#                                                                                                            #
# PROCESSING INFILES                                                                                         #
#                                                                                                            #
##############################################################################################################

### first file: FASTA1 ################################################
# rewriting and filtering
tr -d ' \15\32' < $FASTA1 |
  $BAWK -v m=$MINLGT '/^>/{if(length(s)>=m){print toupper(s)}
                           s="";
                           next;
                          }
                          {s=s""$0}
                      END {if(length(s)>=m){print toupper(s)}}' |
    tr -c 'ACGT' '\n' |
      $BAWK -v m=$MINLGT 'BEGIN        {x=1000000-1}
                          (length()>=m){print">"(++x); print}' > $SEQ1 ;

LGT1=$(grep -v "^>" $SEQ1 | tr -cd 'ACGT' | wc -c);
if [ $LGT1 -lt $MINLGT ]
then
  echo "[ERROR] sequence too short: $FASTA1" >&2 ;
  finalize ;
fi

# building segments
grep -v "^>" $SEQ1 |
  fold -w $WLGT | 
    $BAWK -v m=$MINLGT 'BEGIN        {x=1000000-1}
                        (length()>=m){print">"(++x); print}' > $SFRAG1 ;
NFRAG1=$(grep -c "^>" $SFRAG1);

if $VERBOSE
then
  echo ;
  echo "# query file" ;
  echo "> name:             $FASTA1" ;
  echo "> length:           $LGT1 bps" ;
  echo "> no. segments:     $NFRAG1" ;
fi

if [ "$OUTFILE" != "$NA" ]
then
  echo -e "#file1\tfile2\tnfrag1\tnfrag2\tnfragRBH\tp-distance\tANI" > $OUTFILE ;
fi


### next file(s): FASTA2 ##############################################
for FASTA2 in $FLIST
do
  if ! $VERBOSE ; then echo -e -n "[          ]" >&2 ; fi
    
  # rewriting and filtering
  tr -d ' \15\32' < $FASTA2 |
    $BAWK -v m=$MINLGT '/^>/{if(length(s)>=m){print toupper(s)}
                             s="";
                             next;
                            }
                            {s=s""$0}
                        END {if(length(s)>=m){print toupper(s)}}' |
      tr -c 'ACGT' '\n' |
        $BAWK -v m=$MINLGT 'BEGIN        {x=2000000-1}
                            (length()>=m){print">"(++x); print}' > $SEQ2 ;

  LGT2=$(grep -v "^>" $SEQ2 | tr -cd 'ACGT' | wc -c);
  if [ $LGT2 -lt $MINLGT ]; then continue; fi

  # building segments
  grep -v "^>" $SEQ2 |
    fold -w $WLGT | 
      $BAWK -v m=$MINLGT 'BEGIN        {x=2000000-1}
                          (length()>=m){print">"(++x); print}' > $SFRAG2 ;
  NFRAG2=$(grep -c "^>" $SFRAG2);

  if $VERBOSE
  then
    echo ;
    echo "# subject file" ;
    echo "> name:             $FASTA2" ;
    echo "> length:           $LGT2 bps" ;
    echo "> no. segments:     $NFRAG2" ;
  fi

  if ! $VERBOSE ; then echo -e -n "\r            \r[.         ]" >&2 ; fi
  
  # comparing sequence lengths
  if [ $LGT1 -lt $LGT2 ]
  then
    cp $SFRAG1 $SFRAGI &
    cp $SFRAG2 $SFRAGJ &
    cp $SEQ1   $SEQI   &
    cp $SEQ2   $SEQJ   &
    SLGT=$LGT2;
  else
    sed 's/>2/>1/g' $SFRAG2 > $SFRAGI &
    sed 's/>1/>2/g' $SFRAG1 > $SFRAGJ &
    sed 's/>2/>1/g' $SEQ2   > $SEQI   &
    sed 's/>1/>2/g' $SEQ1   > $SEQJ   &
    SLGT=$LGT1;
  fi
  wait ;
  # here, I/J := the shortest/longest genomes

  if ! $VERBOSE ; then echo -e -n "\r            \r[..        ]" >&2 ; fi


  if $VERBOSE
  then
    echo "# first estimate" ;
  fi

  # initial SWIPE options
  SWOPT="$SCORES_INIT --num_threads=$NTHREADS";
  # MINLGT=$(( $MINLGT / 2 ));

  # Smith-Waterman search of SFRAGI against SFRAGJ
  $MAKEBDB -in $SFRAGJ &>/dev/null ;
  # 1----- 2----- 3----- 4----- 5------- 6--- 7----- 8--- 9----- 10-- 11---- 12------
  # qseqid sseqid pident length mismatch gaps qstart qend sstart send evalue bitscore
  $SWIPE --query=$SFRAGI --db=$SFRAGJ $SWOPT |
    sed 's/lcl|//g' |
      $TAWK '(NF==12){print $1"\t"$7"\t"$8"\t"$2"\t"$9"\t"$10"\t"(100*(1.0-$5/($4-$6)))"\t"$5"\t"$6"\t"$4"\t"$12}
             (NF==11){print $1"\t"$7"\t"$8"\t"$2"\t"$9"\t"$10"\t"(100*(1.0-$5/($4-$6)))"\t"$5"\t"$6"\t"$4"\t"$11}' > $TMP ;
  # 1----- 2----- 3--- 4----- 5----- 6--- 7----- 8------- 9--- 10---- 11------
  # qseqid qstart qend sseqid sstart send pident mismatch gaps length bitscore
  sort -k1,1n -k11,11gr $TMP |
    $TAWK -v m=$MINLGT '($10-$9<m){next}
                        ($1!=id)  {print;id=$1;bs=$11;next}
                        ($11==bs) {print}' > $TABIJ ;

  if ! $VERBOSE ; then echo -e -n "\r            \r[....      ]" >&2 ; fi

  # Smith-Waterman search of SFRAGJ against SFRAGI
  $MAKEBDB -in $SFRAGI &>/dev/null &  # <= formatting SFRAGI
  cut -f4 $TABIJ > $TMP ;                                          #    | selecting from SFRAGJ only those
  paste - - < $SFRAGJ | grep -F -f $TMP | tr '\t' '\n' > $TABJI ;  # <= | segments corresponding to a hit
  mv $TABJI $SFRAGJ ;                                              #    | during the previous SW search
  wait ;
  # 1----- 2----- 3----- 4----- 5------- 6--- 7----- 8--- 9----- 10-- 11---- 12------
  # qseqid sseqid pident length mismatch gaps qstart qend sstart send evalue bitscore
  $SWIPE --query=$SFRAGJ --db=$SFRAGI $SWOPT |
    sed 's/lcl|//g' |
      $TAWK '(NF==12){print $1"\t"$7"\t"$8"\t"$2"\t"$9"\t"$10"\t"(100*(1.0-$5/($4-$6)))"\t"$5"\t"$6"\t"$4"\t"$12}
             (NF==11){print $1"\t"$7"\t"$8"\t"$2"\t"$9"\t"$10"\t"(100*(1.0-$5/($4-$6)))"\t"$5"\t"$6"\t"$4"\t"$11}' > $TMP ;
  # 1----- 2----- 3--- 4----- 5----- 6--- 7----- 8------- 9--- 10---- 11------
  # qseqid qstart qend sseqid sstart send pident mismatch gaps length bitscore
  sort -k1,1n -k11,11gr $TMP |
    $TAWK -v m=$MINLGT '($10-$9<m){next}
                        ($1!=id)  {print;id=$1;bs=$11;next}
                        ($11==bs) {print}' > $TABJI ;

  if ! $VERBOSE ; then echo -e -n "\r            \r[......    ]" >&2 ; fi

  # RBH analysis
  sort -k4,4n $TABJI |
    $TAWK '{print$4"\t"$1}' > $TMP ;
  join -j 1 $TABIJ $TMP |
    tr ' ' '\t' |
      $TAWK '($4==$12)' |
        cut -f1-11 |
          $TAWK '($1!=id){id=$1;print}' > $TABRBH ;
  NFRAGRBH=$(cat $TABRBH | wc -l);
  
  # first estimates
  #  |rbh| = no. RBH
  #  simi := 100 * (1 - [sum_rbh (mismatch)] / [sum_rbh (length-gaps)])
  #  dist := 1 - simi / 100
  #  oani := 100 * [sum_rbh ((length-gap-mismatch)/(length-gaps))] / |rbh|
  line="0 0 0 0";
  [ -s $TABRBH ] && line="$($TAWK '   {m+=$8; l+=($10-$9); a+=($10-$9-$8)/($10-$9)}
                                   END{print(100*(1-m/l))" "m" "l" "(100*a/NR)}' $TABRBH)";
  core=$($BAWK '{print$3}' <<<"$line");
  qcov=$($BAWK -v l=$LGT1 -v c=$core 'BEGIN{print (100*c/l)}');
  scov=$($BAWK -v l=$LGT2 -v c=$core 'BEGIN{print (100*c/l)}');
  pcov=$($BAWK -v l1=$LGT1 -v l2=$LGT2 -v c=$core 'BEGIN{print (100*c/(l1+l2-c))}');
  mism=$($BAWK '{print$2}' <<<"$line");
  simi=$($BAWK '{print$1}' <<<"$line");
  dist=$($BAWK -v s=$simi 'BEGIN{printf("%.6f",1-s/100)}');
  oani=$($BAWK '{printf("%.4f",$4)}' <<<"$line");
  if $VERBOSE
  then
    echo "> no. RBH:          $NFRAGRBH" ;
  # echo "> core size:        $core bps" ;
  # echo "> query overlap:    $qcov %" ;
  # echo "> subject overlap:  $scov %" ;
  # echo "> pairwise overlap: $pcov %" ;
  # echo "> no. mismatches:   $mism" ;
    echo "> p-distance:       $dist" ;
    echo "> %ANI:             $oani" ;
  fi

  
  if $VERBOSE
  then
    echo "# final estimate" ;
  fi
  
  if ! $VERBOSE ; then echo -e -n "\r            \r[.......   ]" >&2 ; fi

  # gathering RBH segments from SFRAGI
  cut -f1 $TABRBH > $TMP ;
  paste - - < $SFRAGI | grep -F -f $TMP > $SEQI ;
  mv $SEQI $SFRAGI ;
  
  # gathering RBH segments from SFRAGJ
  cut -f4 $TABRBH > $TMP ;
  paste - - < $SFRAGJ | grep -F -f $TMP | tr '\t' '\n' > $SEQJ ;
  mv $SEQJ $SFRAGJ ;

  # formatting selected SFRAGJ using a fake taxid_map
  $TAWK '{print$1"\t"$1}' $TMP > $TAX ;
  $MAKEBDB -in $SFRAGJ -taxid_map $TAX &>/dev/null ;

  if ! $VERBOSE ; then echo -e -n "\r            \r[........  ]" >&2 ; fi

  # estimating pairwise similarity using dedicated scores
  while IFS=$'\t' read -r qseqid _ _ sseqid sstart send pident mismatch gaps length _
  do
    # gathering qseqid
    grep -m 1 -F ">$qseqid" $SFRAGI | tr '\t' '\n' > $FRAGI ;

    # SWIPE options from pairwise similarity     #   observed    theoretical
    simi=$(sed 's/\..*//' <<<"$pident");         #   ----------  ----------- 
                        SWOPT="$SCORES_98_100";  #   [98 - 100]  [98 - 100] 
    [ $simi -lt 98 ] && SWOPT="$SCORES_96_98";   #   [96 - 98 [  [96 - 98 [ 
    [ $simi -lt 96 ] && SWOPT="$SCORES_93_96";   #   [93 - 96 [  [93 - 96 [
    [ $simi -lt 93 ] && SWOPT="$SCORES_90_93";   #   [90 - 93 [  [90 - 93 [ 
    [ $simi -lt 90 ] && SWOPT="$SCORES_85_90";   #   [85 - 90 [  [85 - 90 [
    [ $simi -lt 85 ] && SWOPT="$SCORES_80_85";   #   [79 - 85 [  [80 - 85 [ 
    [ $simi -lt 79 ] && SWOPT="$SCORES_75_80";   #   [73 - 79 [  [75 - 80 [
    [ $simi -lt 73 ] && SWOPT="$SCORES_70_75";   #   [67 - 73 [  [70 - 75 [ 
    [ $simi -lt 67 ] && SWOPT="$SCORES_65_70";   #   [61 - 67 [  [65 - 70 [
    [ $simi -lt 61 ] && SWOPT="$SCORES_60_65";   #   [55 - 61 [  [60 - 65 [ 
    [ $simi -lt 55 ] && SWOPT="$SCORES_55_60";   #   [49 - 55 [  [55 - 60 [   
    [ $simi -lt 49 ] && SWOPT="$SCORES_0_55";    #   [0  - 49 [  [0  - 55 [ 
    [ $sstart -lt $send ] && s=1 || s=2 ;
    SWOPT="$SWOPT --strand=$s"
  
    # restricting to sseqid
    echo "$sseqid" > $TAX ;

    # Smith-Waterman local alignment between qseqid and sseqid
    # 1----- 2----- 3----- 4----- 5------- 6--- 7----- 8--- 9----- 10-- 11---- 12------
    # qseqid sseqid pident length mismatch gaps qstart qend sstart send evalue bitscore
    $SWIPE --query=$FRAGI --db=$SFRAGJ $SWOPT -x $TAX |
      sed 's/lcl|//g' |
        $TAWK '(NR>1)  {next}
               (NF==12){print $1"\t"$7"\t"$8"\t"$2"\t"$9"\t"$10"\t"(100*(1.0-$5/($4-$6)))"\t"$5"\t"$6"\t"$4"\t"$12}
               (NF==11){print $1"\t"$7"\t"$8"\t"$2"\t"$9"\t"$10"\t"(100*(1.0-$5/($4-$6)))"\t"$5"\t"$6"\t"$4"\t"$11}' ;
    # 1----- 2----- 3--- 4----- 5----- 6--- 7----- 8------- 9--- 10---- 11------
    # qseqid qstart qend sseqid sstart send pident mismatch gaps length bitscore
     
  done < $TABRBH > $TMP ;

  if ! $VERBOSE ; then echo -e -n "\r            \r[......... ]" >&2 ; fi

  # final estimates
  #  |rbh| = no. RBH
  #  simi := 100 * (1 - [sum_rbh (mismatch)] / [sum_rbh (length-gaps)])
  #  dist := 1 - simi / 100
  #  oani := 100 * [sum_rbh ((length-gap-mismatch)/(length-gaps))] / |rbh|
  line="0 0 0 0";
  [ -s $TMP ] && line="$($TAWK '   {m+=$8; l+=($10-$9); a+=($10-$9-$8)/($10-$9)}
                                END{print(100*(1-m/l))" "m" "l" "(100*a/NR)}' $TMP)";
  core=$($BAWK '{print$3}' <<<"$line");
  qcov=$($BAWK -v l=$LGT1 -v c=$core 'BEGIN{print (100*c/l)}');
  scov=$($BAWK -v l=$LGT2 -v c=$core 'BEGIN{print (100*c/l)}');
  pcov=$($BAWK -v l1=$LGT1 -v l2=$LGT2 -v c=$core 'BEGIN{print (100*c/(l1+l2-c))}');
  mism=$($BAWK '{print$2}' <<<"$line");
  simi=$($BAWK '{print$1}' <<<"$line");
  dist=$($BAWK -v s=$simi 'BEGIN{printf("%.6f",1-s/100)}');
  oani=$($BAWK '{printf("%.4f",$4)}' <<<"$line");
  if $VERBOSE
  then
  # echo "> core size:        $core bps" ;
  # echo "> query overlap:    $qcov %" ;
  # echo "> subject overlap:  $scov %" ;
  # echo "> pairwise overlap: $pcov %" ;
  # echo "> no. mismatches:   $mism" ;
    echo "> p-distance:       $dist" ;
    echo "> %ANI:             $oani" ;
  else
    echo -e -n "\r            \r" >&2 ;
    echo -e "$(basename $FASTA1)\t$(basename $FASTA2)\t$NFRAG1\t$NFRAG2\t$NFRAGRBH\t$dist\t$oani" ;
  fi
  
  if [ "$OUTFILE" != "$NA" ]
  then
    echo -e "$(basename $FASTA1)\t$(basename $FASTA2)\t$NFRAG1\t$NFRAG2\t$NFRAGRBH\t$dist\t$oani" >> $OUTFILE ;
  fi
done

finalize ;

exit ;


